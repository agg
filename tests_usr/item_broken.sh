#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

feed_exists() { assert_exists feed; }
item_exists() { assert_exists feed/item\ title; }
item_missing()  { assert_missing feed/item; }

echo "Running agg on feed with item title after description..."
agg_run title_after_description.rss
t feed_exists
t item_exists

echo "Running agg on feed containing item with duplicate titles..."
agg_fail item_duplicate_title.rss
t item_missing

echo "Running agg on feed containing item with duplicate description..."
agg_fail item_duplicate_description.rss
t item_missing

echo "Running agg on feed containing item with duplicate link..."
agg_fail item_duplicate_link.rss
t item_missing

echo "Running agg on feed containing item with duplicate date..."
agg_fail item_duplicate_date.rss
t item_missing
