#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

feed_exists_f()      { assert_exists "feed '\" feed!?"; }
feed_item_exists_f() { assert_exists "feed '\" feed!?/item"; }
feed_item_delete_f() { assert_delete "feed '\" feed!?/item"; } 

feed_item_exists_t() { assert_exists "feed/item '\" !?"; }
feed_item_delete_t() { assert_delete "feed/item '\" !?"; } 

echo "Running agg on feed with special characters in title..."
agg_run special_f.rss
t feed_exists_f
t feed_item_exists_f
t feed_item_delete_f

echo "Running agg on feed with special characters in item title..."
agg_run special_t.rss
t feed_item_exists_t
t feed_item_delete_t

## FIXME: The following feed could not be handled by agg,
##        because date after desc without title.
##        write a acceptance test for this case (that
##        either fails or passes).
#echo "Running agg on feed with special characters in item description..."
#agg_run special_d.rss
#t feed_item_exists_t
#t feed_item_delete_t
