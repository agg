#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

feed_exists()  { assert_exists feed; }
feed_empty()   { assert_empty  feed; }
feed_date()    { assert_date   feed 1286052203; }
item1_exists() { assert_exists "feed/Item 1"; }
item2_exists() { assert_exists "feed/Item 2"; }
item3_exists() { assert_exists "feed/Item 3"; }
item1_date()   { assert_date   "feed/Item 1" 1286052203; }
item2_date()   { assert_date   "feed/Item 2" 1270203000; }
item3_date()   { assert_date   "feed/Item 3" 1270119246; }
item1_title()  { assert_value  "feed/Item 1" title "Item 1"; }
item2_title()  { assert_value  "feed/Item 2" title "Item 2"; }
item3_title()  { assert_value  "feed/Item 3" title "Item 3"; }
item1_desc()   { assert_value  "feed/Item 1" desc  "Random item."; }
item2_desc()   { assert_value  "feed/Item 2" desc  "Not so random item."; }
item3_desc()   { assert_value  "feed/Item 3" desc  "No item."; }
item1_link()   { assert_value  "feed/Item 1" link  "/dev/random"; }
item2_link()   { assert_value  "feed/Item 2" link  "/dev/urandom"; }
item3_link()   { assert_value  "feed/Item 3" link  "/dev/null"; }

test_sample()
{
	echo "Running agg on sample feed ($1) (directory missing)..."
	agg_run "sample_$1.rss"
	t feed_exists
	t feed_date
	t item1_exists
	t item2_exists
	t item3_exists
	t item1_date
	t item2_date
	t item3_date
	t item1_title
	t item2_title
	t item3_title
	t item1_desc
	t item2_desc
	t item3_desc
	t item1_link
	t item2_link
	t item3_link
	
	echo "Running agg sample feed ($1) (up to date) ..."
	agg_run "sample_$1.rss"
	t feed_exists
	t feed_date
	t item1_exists
	t item2_exists
	t item3_exists
	t item1_date
	t item2_date
	t item3_date
	t item1_title
	t item2_title
	t item3_title
	t item1_desc
	t item2_desc
	t item3_desc
	t item1_link
	t item2_link
	t item3_link
	
	echo "Deleting old news..."
	agg_clean feed
	t feed_exists
	t feed_date
	t feed_empty
	
	echo "Running agg on sample feed ($1) (directory up to date but empty)..."
	agg_run "sample_$1.rss"
	t feed_exists
	t feed_date
	t feed_empty
	
	echo "Changing mtime..."
	touch -md "1970-01-01 00:00:00.000000000 +0000" feed
	
	echo "Running agg on sample feed ($1) (directory outdated and empty)..."
	agg_run "sample_$1.rss"
	t feed_exists
	t feed_date
	t item1_exists
	t item2_exists
	t item3_exists
	t item1_date
	t item2_date
	t item3_date
	t item1_title
	t item2_title
	t item3_title
	t item1_desc
	t item2_desc
	t item3_desc
	t item1_link
	t item2_link
	t item3_link
}

test_sample descending
rm -rf feed
test_sample ascending
