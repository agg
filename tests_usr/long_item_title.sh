#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

ORIG=_________________________________________________________________
TRNC=_____________________________________________________________...

feed_exists()   { assert_exists  feed; }
orig_missing()  { assert_missing feed/$ORIG; }
item_exists()   { assert_exists  feed/$TRNC; }
item_contents() { assert_value   feed/$TRNC title $ORIG; }

echo "Running agg on feed with long item title..."
agg_run long_item_title.rss
t feed_exists
t orig_missing
t item_exists
t item_contents
