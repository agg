#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

feed_exists()     { assert_exists \\feed; }
item_exists()     { assert_exists \\feed/item; }

echo "Running agg on evil feed (feed title)..."
agg_run sanity_f.rss
t feed_exists
t item_exists
