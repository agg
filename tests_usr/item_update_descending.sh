#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

feed_exists() { assert_exists feed; }
feed_date()   { assert_date   feed      1286052203; }
item_exists() { assert_exists feed/item; }
item_date()   { assert_date   feed/item 1286052203; }
item_desc()   { assert_value  feed/item desc second; }


echo "Running agg on feed containing updated item (descending order)..."
agg_run item_update_descending.rss
t feed_exists
t feed_date
t item_exists
t item_desc
t item_date
