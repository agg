#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

feed_exists()   { assert_exists feed; }
item_exists()   { assert_exists feed/item; }
item_date()     { assert_date   feed/item 0; }
item_contents() { assert_value  feed/item title item; }

echo "Running agg on simple feed (titles)..."
agg_run simple_t.rss
t feed_exists
t item_exists
t item_date
t item_contents
