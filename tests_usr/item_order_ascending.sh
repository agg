#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

feed_exists()  { assert_exists feed; }
feed_empty()   { assert_empty  feed; }
feed_date()    { assert_date   feed 1286052203; }
item1_exists() { assert_exists "feed/Item 1"; }
item2_exists() { assert_exists "feed/Item 2"; }

mkdir feed
touch -md "2010-04-02 12:09:59.000000000 +0200" feed

echo "Running agg on feed with item order ascending..."
agg_run item_order_ascending.rss
t feed_exists
t feed_date
t item1_exists
t item2_exists
