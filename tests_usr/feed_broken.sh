#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

. ./libtests.sh

feed_missing()  { assert_missing feed; }
feed_exists()   { assert_exists  feed; }
item_missing()  { assert_missing feed/item; }

echo "Running agg on feed without title..."
BEFORE="`ls`"
agg_fail no_feed_title.rss
[ "$BEFORE" != "`ls`" ] && exit 1

echo "Running agg on feed with title after items..."
agg_fail feed_title_after_items.rss
t feed_missing

echo "Running agg on feed with two titles..."
agg_fail feed_two_titles.rss
t feed_missing
