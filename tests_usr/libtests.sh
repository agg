#!/bin/sh
# Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
#
#                 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#       TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
# 0. You just DO WHAT THE FUCK YOU WANT TO.

#set -x

function agg_run()
{
	(cat "$1" | ../src/agg)
	res=$?
	if [ $res -ne 0 ]; then
		echo "Failed to run agg ($res)."
		exit $res
	fi
}

function agg_fail()
{
	(cat "$1" | ../src/agg)
	res=$?
	if [ $res -eq 0 ]; then
		echo "Failed to fail running agg."
		exit 1
	fi
}

function agg_clean()
{
	find "$1" -mindepth 1 \
	          -maxdepth 1 \
	          -type d \
	          -exec ../src/nomtime "rm -rf" '{}' \;
	res=$?
	if [ $res -ne 0 ]; then
		echo "Failed to run nomtime."
		exit $res
	fi
}

function t()
{
	printf "  %-32s: " "$1"
	$1
	result=$?
	if [ $result -ne 0 ]; then
		echo "FAIL"
		exit $result
	else
		echo "OK"
	fi
}

function assert_exists()  { [ -d "$1" ]; }
function assert_date()    { [ `stat -c %Y "$1"` = "$2" ]; }
function assert_empty()   { [ `ls "$1" | wc -l` = "0" ]; }
function assert_missing() { [ ! -e "$1" ]; }
# BUG: "`cat`" drops trailing \n.
function assert_value()   { [ "`cat \"$1/$2\"`" = "$3" ]; }
function assert_delete()  {
	BASENAME="`dirname "$1"`"
	OLD_DATE="`stat -c %Y "$BASENAME"`"
	../src/nomtime "rm -rf" "$1"
	assert_date "$BASENAME" "$OLD_DATE"
}
