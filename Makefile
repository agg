-include Make.config

MAN=agg.1

all:
	make -C src

tests: all
	make -C tests_dev run && make -C tests_usr run

install: all
	make -C src install
	$(INST) -m 444 $(MAN) $(MANDIR)/man1

uninstall:
	make -C src uninstall

clean:
	make -C src clean
	make -C tests_dev clean
