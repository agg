/* agg - the news aggregator
 * Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#define _XOPEN_SOURCE
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include "config.h"
#include "bool.h"
#include "fail.h"
#include "fs.h"

char   feed_title[TEXT_BUFFER_SIZE] = { 0 };
time_t feed_date = 0;
time_t feed_date_new = 0;

char   item_title[TEXT_BUFFER_SIZE] = { 0 };
char   item_link [TEXT_BUFFER_SIZE] = { 0 };
char   item_desc [TEXT_BUFFER_SIZE] = { 0 };
time_t item_date = 0;

void fs_buffer(char* dest, const char* src, size_t size)
{
	assert(dest);
	assert(src);
	assert(size);

	strncpy(dest, src, size);

	/* Destination is not null-terminated iff
	 * strlen(src) >= size. */
	if (dest[size - 1] != '\0') {
		dest[size - 4] = '.';
		dest[size - 3] = '.';
		dest[size - 2] = '.';
		dest[size - 1] = '\0';
	}

	assert(strlen(dest) < size);
}

void sanitize(char* str)
{
	char* evil;
	while ((evil = strchr(str, '/'))) *evil = '\\';
}

void set_item_date(const char* date)
{
	struct tm t;

	assert(!item_date);

	assert(strptime(date, "%a, %d %b %Y %T %z", &t));
	item_date = mktime(&t);
	assert(item_date != -1);
}

void set_item_link(const char* link)
{
	assert(!item_link[0]);
	fs_buffer(item_link, link, TEXT_BUFFER_SIZE);
}

void set_item_desc(const char* desc)
{
	assert(!item_desc[0]);
	fs_buffer(item_desc, desc, TEXT_BUFFER_SIZE);
}

void set_item_title(const char* title)
{
	assert(!item_title[0]);
	fs_buffer(item_title, title, TEXT_BUFFER_SIZE);
}

const char* get_item_title()
{
	return item_title;
}

void feed_open(const char* title)
{
	struct stat st;

	if (feed_title[0]) {
		/* Feed contained title twice. The
		 * directory may just have been created.
		 * Deletion may lead to loss of previous
		 * mtime. We don't care; that feed is
		 * fucked up anyways. */
		chdir("..");
		rmdir(feed_title);
		fail(111);
	}

	fs_buffer(feed_title, title, TEXT_BUFFER_SIZE);
	sanitize(feed_title);

	if (!stat(feed_title, &st)) feed_date = st.st_mtime;
	mkdir(feed_title, 0755);
	assert(!chdir(feed_title));
}



void feed_flush()
{
	struct timeval times[2];
	times[0].tv_sec  = feed_date_new;
	times[0].tv_usec = 0;
	times[1].tv_sec  = feed_date_new;
	times[1].tv_usec = 0;

	if (!feed_title[0]) return;

	assert(!chdir(".."));
	assert(!utimes(feed_title, times));
	feed_title[0] = '\0';
}


void fs_set(const char* name, const char* value)
{
	FILE* f;

	assert(name);
	assert(value);

	/* Only write if attribute has a value. */
	if (value[0]) {
		assert(f = fopen(name, "w"));
		fprintf(f, "%s", value);
		fclose(f);
	}
}

void item_flush()
{
	struct timeval times[2];
	struct stat    st;
	char buf[FILE_NAME_LENGTH + 1] = { 0 };
	const char* name_src;
	assert(feed_title[0]);

	/* snip: from set_item_date */
	/* assert(item_title[0]); */
	/*if (item_date > 0 && item_date <= feed_date) {
		printf("No new feeds assumed.\n");
		fail(0);
	} else */if (item_date > feed_date_new) {
		feed_date_new = item_date;
	}
	/* snap */

	if (item_date == 0 || item_date > feed_date) {
		name_src = item_title[0] ? item_title : item_desc;
		assert(name_src[0]);
		fs_buffer(buf, name_src, FILE_NAME_LENGTH + 1);
		sanitize(buf);

		if (mkdir(buf, 0755)) {
			assert(errno == EEXIST);

			stat(buf, &st);
			if (st.st_mtime >= item_date) return;
		}

		assert(chdir(buf) == 0);
		fs_set("title", item_title);
		fs_set("desc",  item_desc);
		fs_set("link", item_link);
		assert(chdir("..") == 0);

		/* FIXME: item date might not have been set. */

		times[0].tv_sec  = item_date;
		times[0].tv_usec = 0;
		times[1].tv_sec  = item_date;
		times[1].tv_usec = 0;

		assert(!utimes(buf, times));
	}

	item_title[0] = 0;
	item_desc[0] = 0;
	item_link[0] = 0;
	item_date = 0;
}
