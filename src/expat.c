/* agg - the news aggregator
 * Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdio.h>
#include "expat.h"
#include "stack.h"
#include "text.h"

bool expat_use_text_buffer = false;

void XMLCALL expat_text(void* data, const XML_Char* s, int len)
{
	(void) data;

	if (!expat_use_text_buffer) return;

	text_buffer(s, len * sizeof(XML_Char));
}

void XMLCALL expat_enter(void* data, const char* elem, const char** attr)
{
	(void) data;

	stack_top()->enter(elem, attr);
}

void XMLCALL expat_leave(void* data, const char* elem)
{
	(void) data;

	stack_top()->leave(elem);
}

void expat_setup(XML_Parser* p)
{
	XML_SetCharacterDataHandler(*p, expat_text);
	XML_SetElementHandler(*p, expat_enter, expat_leave);
}
