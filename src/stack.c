/* agg - the news aggregator
 * Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <string.h>
#include <assert.h>
#include "stack.h"
#include "fail.h"
#include "rss.h"

#define STACK_LAYERS 3

void unknown_enter(const char* elem, const char** attr);
void unknown_leave(const char* elem);

struct Layer _stack[STACK_LAYERS] = { { unknown_enter, unknown_leave } };
unsigned int _top = 0;

/* FIXME init */

const struct Layer* stack_top()
{
	assert(_top < STACK_LAYERS);
	/* FIXME */
	/* assert(_stack[_top].enter) */
	/* assert(_stack[_top].leave) */

	return &_stack[_top];
}

void stack_next()
{
	assert(_top < STACK_LAYERS - 1);
	++_top;
}

void stack_prev()
{
	assert(_top > 0);
	--_top;
}

void stack_set(unsigned i, struct Layer l)
{
	assert(i < STACK_LAYERS);

	_stack[i].enter = l.enter;
	_stack[i].leave = l.leave;
}

void unknown_enter(const char* elem, const char** attr)
{
	(void) attr;

	assert(!strcmp("rss", elem));

	rss_setup();
}

void unknown_leave(const char* elem)
{
	(void) elem;

	assert(0);
}
