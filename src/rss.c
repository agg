/* agg - the news aggregator
 * Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <string.h>
#include "stack.h"
#include "text.h"
#include "fail.h"
#include "rss.h"
#include "fs.h"

void rss_global_enter(const char* elem, const char** attr)
{
	(void) attr;

	if (strcmp("channel", elem) != 0) fail(ERR_INVALID);
	stack_next();
}

void rss_global_leave(const char* elem)
{
	if (strcmp("rss", elem) != 0) fail(ERR_INVALID);
}

void rss_channel_enter(const char* elem, const char** attr)
{
	(void) attr;

	if (strcmp("item", elem) == 0) {
		stack_next();
	} else if (strcmp("title", elem) == 0) {
		text_enable();
	}
}

void rss_channel_leave(const char* elem)
{
	if (strcmp("channel", elem) == 0) {
		feed_flush();
		stack_prev();
	} else if (strcmp("title", elem) == 0) {
		feed_open(text_get());
		text_disable();
	}
}

void rss_item_enter(const char* elem, const char** attr)
{
	(void) attr;

	if (strcmp("title", elem) == 0) {
		text_enable();
	} else if (strcmp("link", elem) == 0) {
		text_enable();
	} else if (strcmp("pubDate", elem) == 0) {
		text_enable();
	} else if (strcmp("description", elem) == 0) {
		text_enable();
	}
}

void rss_item_leave(const char* elem)
{
	if (strcmp("item", elem) == 0) {
		item_flush();
		stack_prev();
	} else if (strcmp("title", elem) == 0) {
		set_item_title(text_get());
		text_disable();
	} else if (strcmp("link", elem) == 0) {
		set_item_link(text_get());
		text_disable();
	} else if (strcmp("pubDate", elem) == 0) {
		set_item_date(text_get());
		text_disable();
	} else if (strcmp("description", elem) == 0) {
		set_item_desc(text_get());
		text_disable();
	}
}

void rss_setup()
{
	stack_set(0, layer(rss_global_enter,  rss_global_leave));
	stack_set(1, layer(rss_channel_enter, rss_channel_leave));
	stack_set(2, layer(rss_item_enter,    rss_item_leave));
}
