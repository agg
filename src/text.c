/* agg - the news aggregator
 * Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <string.h>
#include <assert.h>
#include "config.h"
#include "stack.h"
#include "expat.h"
#include "text.h"

char buffer[TEXT_BUFFER_SIZE] = { 0 };
char* cursor = buffer;

const char* text_get()
{
	return buffer;
}

void text_buffer(const char* str, size_t len)
{
	/* We want to append a trailing \0, so we have one
	 * element less. Beware, this will underflow! */
	size_t free = buffer + TEXT_BUFFER_SIZE - cursor - 1;
	size_t n   = len < free ? len : free;

	/* Already full? Prevent overflow in memcpy(). */
	if (cursor == buffer + TEXT_BUFFER_SIZE) return;

	assert(cursor + n < buffer + TEXT_BUFFER_SIZE);
	memcpy(cursor, str, n);

	cursor += n;
	*cursor = 0;
}

void text_enable()
{
	expat_use_text_buffer = true;

	cursor = buffer;
	*cursor = 0;
}

void text_disable()
{
	expat_use_text_buffer = false;

	cursor = buffer;
	*cursor = 0;
}
