#include "fs.h"
#include "../src/fs.h"
#include "../src/config.h"
#include <string.h>

int test_fs_buffer_sanity()
{
	char buf[TEXT_BUFFER_SIZE];
	fs_buffer(buf, "../../foo", TEXT_BUFFER_SIZE);
	return strcmp("../../foo", buf) == 0;
}

int test_fs_buffer_fill_minus_one()
{
	char exp[TEXT_BUFFER_SIZE] = { 0 };
	char buf[TEXT_BUFFER_SIZE] = { 0 };

	memset(exp, ' ', TEXT_BUFFER_SIZE - 2);
	fs_buffer(buf, exp, TEXT_BUFFER_SIZE);

	return strcmp(exp, buf) == 0;
}

int test_fs_buffer_fill()
{
	char exp[TEXT_BUFFER_SIZE] = { 0 };
	char buf[TEXT_BUFFER_SIZE] = { 0 };

	memset(exp, ' ', TEXT_BUFFER_SIZE - 1);
	fs_buffer(buf, exp, TEXT_BUFFER_SIZE);

	return strcmp(exp, buf) == 0;
}

int test_fs_buffer_fill_plus_one()
{
	char exp[TEXT_BUFFER_SIZE]     = { 0 };
	char buf[TEXT_BUFFER_SIZE]     = { 0 };
	char src[TEXT_BUFFER_SIZE + 1] = { 0 };

	memset(exp, ' ', TEXT_BUFFER_SIZE - 1);
	memset(src, ' ', TEXT_BUFFER_SIZE);

	exp[TEXT_BUFFER_SIZE - 4] = '.';
	exp[TEXT_BUFFER_SIZE - 3] = '.';
	exp[TEXT_BUFFER_SIZE - 2] = '.';

	fs_buffer(buf, src, TEXT_BUFFER_SIZE);

	return strcmp(exp, buf) == 0;
}

int test_fs_item_set_title()
{
	set_item_title("foo");
	return 1;
}

int test_fs_item_get_title()
{
	return strcmp("foo", get_item_title()) == 0;
}
