/* agg - the news aggregator
 * Copyright (C) 2011 Andreas Waidler <arandes@programmers.at>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "../src/config.h"
#include "../src/layer.h"

#include "fs.h"
#include "text.h"
#include "stack.h"

#define TEST(fun) test(#fun, fun())
void test(const char* expr, int res)
{
	printf("  %-32s: %s\n", expr, res ? "OK" : "FAIL");
	if (!res) exit(1);
}

int main()
{
	TEST(test_text_is_empty);
	TEST(test_text_enable);
	TEST(test_text_disable);
	TEST(test_text_write);
	TEST(test_text_clear);
	TEST(test_text_append);
	TEST(test_text_fill);

	TEST(test_stack_set_top);
	TEST(test_stack_next_prev);
	TEST(test_stack_set_next);

	TEST(test_fs_buffer_sanity);
	TEST(test_fs_buffer_fill_minus_one);
	TEST(test_fs_buffer_fill);
	TEST(test_fs_buffer_fill_plus_one);

	TEST(test_fs_item_set_title);
	TEST(test_fs_item_get_title);

	return 0;
}
