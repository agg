#include "text.h"
#include "../src/text.h"

/* buffer must be >= text buffer. */
#define TESTS_BUFSIZE (1024 * 1024)

int test_text_is_empty()
{
	return strcmp("", text_get()) == 0;
}

int test_text_enable()
{
	text_enable();
	return 1;
}

int test_text_disable()
{
	text_disable();
	return 1;
}

int test_text_write()
{
	text_buffer("foobar", 3);
	return strcmp("foo", text_get()) == 0;
}

int test_text_clear()
{
	text_enable();
	if (!test_text_is_empty()) return 0;

	test_text_write();
	text_disable();
	return test_text_is_empty();
}

int test_text_append()
{
	text_buffer("foobar", 3);
	text_buffer("foobar", 3);
	return strcmp("foofoo", text_get()) == 0;
}

int test_text_fill()
{
	char buf[TESTS_BUFSIZE];
	memset(buf, ' ', TESTS_BUFSIZE);
	text_buffer(buf, TESTS_BUFSIZE);

	return strncmp(buf, text_get(), TESTS_BUFSIZE) != 0;
}
