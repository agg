#include "stack.h"
#include "../src/stack.h"

int test_stack_set_top()
{
	struct Layer mock;
	mock.enter = (void (*)(const char*, const char**)) 0x7E57AB1E;
	mock.leave = (void (*)(const char*)) 0xCA11AB1E;

	stack_set(0, mock);
	return stack_top()->enter == mock.enter
	    && stack_top()->leave == mock.leave;
}

int test_stack_next_prev()
{
	struct Layer mock;
	mock.enter = (void (*)(const char*, const char**)) 0x7E57AB1E;
	mock.leave = (void (*)(const char*)) 0xCA11AB1E;

	/* Mock still set from method before. */

	stack_next();
	stack_prev();

	return stack_top()->enter == mock.enter
	    && stack_top()->leave == mock.leave;
}

int test_stack_set_next()
{
	struct Layer mock;
	mock.enter = (void (*)(const char*, const char**)) 0x7E57AB1E;
	mock.leave = (void (*)(const char*)) 0xCA11AB1E;

	stack_set(1, mock);
	stack_next();
	return stack_top()->enter == mock.enter
	    && stack_top()->leave == mock.leave;
}
